<?php

namespace Automovil;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    protected $fillable = ['modelo','anio','marca','color','matricula','slug','foto'];
    
    public function getRouteKeyName()
	{
	    return 'slug';
	}
}


