<?php

namespace Automovil\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modelo' => 'required',
            'foto'=>'required',
            'marca'=>'required',
            'anio'=>'required|numeric',
            'matricula'=>'required'
        ];
    }
}
