<?php

namespace Automovil\Http\Controllers;

use Automovil\Auto;
use File;
use Illuminate\Http\Request;
use Automovil\Http\Requests\StoreAutoRequest;

class AutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $autos = Auto::all();
        return view('autos.index',compact('autos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('autos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAutoRequest $request)
    {
       /*  if($request->hasFile('photo')){ */
            $file = $request->file('foto');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/img/autos/',$name);
       /*  } */
        $auto = new Auto();
        $auto->modelo = $request->input('modelo');
        $auto->marca = $request->input('marca');
        $auto->anio = $request->input('anio');
        $auto->color = $request->input('color');
        $auto->matricula = $request->input('matricula');
        $auto->slug = str_replace(' ', '_', $request->input('matricula'));
        $auto->foto = $name;
        $auto->save();
        return redirect('autos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Auto $auto)
    {
        return view('autos.show',compact('auto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Auto $auto)
    {
        return view('autos.edit',compact('auto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Auto $auto)
    {
        $auto->fill($request->except('foto'));

        if($request->hasFile('foto')){

            $past_photo = public_path().'/img/autos/'.$auto->foto;
            File::delete($past_photo);

            $file = $request->file('foto');
            $name = time().$file->getClientOriginalName();
            $auto->foto = $name;
            $file->move(public_path().'/img/autos/',$name);
        }
        $slug = str_replace(' ', '_', $request->input('matricula'));
        $auto->slug=$slug;
        $auto->save();
        return redirect()->route('autos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auto $auto)
    {
        $fotoBorrada = public_path().'/img/autos/'.$auto->foto;
        File::delete($fotoBorrada);
        $auto->delete();
        return redirect()->route('autos.index');
    }
}
