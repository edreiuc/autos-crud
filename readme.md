<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Detalles de instalacion

Para correr el proyecto previamente debe tenerse instalado composer
para mas detalles vea [instalacion de composer](https://getcomposer.org/download/)

- Cree previamente una base de datos llamada autos.
- Descargue el proyecto de git.
- Ingrese por consola a la carpeta del proyecto y corra el comando: composer install
- Ubique el archivo .env en la raiz del proyecto y configure el puerto de la base de datos, verifique que el nombre de la BD sea correcta.
- nuevamente en consola dentro de la carpeta ejecute el comando: php artisan migrate.
- por ultimo ejecute el comando: php artisan serve
- ingrese en la ruta que le indica la consola


## Conceptos utilizados

- **Implicit binding
- **Form request
- **ORM
- **Laravel Collective
