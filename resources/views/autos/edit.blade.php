@extends('layouts.principal')
    @section('titulo','Editar Auto')
    @section('content')
    {!! Form::model($auto,['route'=>['autos.update',$auto],'method'=>'PUT','files'=>true]) !!}
        @include('autos.form')
        <div class="row">
            <div class="col-md-1">
            {!! Form::submit('Guardar', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    {!! Form::close() !!}
    @endsection