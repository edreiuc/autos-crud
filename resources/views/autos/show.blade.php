@extends('layouts.principal')
    @section('titulo','Auto {{$auto->matricula}}')
    @section('content')
        <div class="row">
            <div class="col-md-8 offset-md-2">
                    <div class="card" >
                        <img class="mx-auto display-block" style="height: 200px; width:200px;" src="{{asset('img/autos').'/'.$auto->foto}}" alt="">
                        <h5 class="text-center">{{$auto->modelo}}</h5>
                        <ul>
                            <li><p><strong>MARCA: </strong> {{$auto->marca}}</p></li>
                            <li><p><strong>MODELO: </strong> {{$auto->modelo}}</p></li>
                            <li><p><strong>AÑO: </strong>{{$auto->anio}}</p></li>
                            <li><p><strong>MATRICULA: </strong> {{$auto->matricula}}</p></li>
                            <li><p><strong>COLOR: </strong> {{$auto->color}}</p></li>
                        </ul>
                        <a class="btn btn-success" href="/autos/{{$auto->slug}}/edit">Editar</a>
                        {!! Form::open(['route'=>['autos.destroy',$auto->slug],'method'=>'DELETE']) !!} 
                            {!! Form::submit('Eliminar', ['class'=>'btn btn-danger','style'=>'width:100%']) !!}                  
                        {!! Form::close() !!}
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="/autos" class="btn btn-primary">Regresar</a>
            </div>
        </div>
    @endsection