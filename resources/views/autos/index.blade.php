@extends('layouts.principal')
    @section('titulo','listado de autos')
    @section('content')
        <h1>Autos de agencia</h1> 
        <a class="btn btn-primary" href="/autos/create">Agregar</a>
        <br><br>
        <div class="row">
            @foreach ($autos as $auto)
                <div class="col-md-3">
                    <div class="card text-center" style="width: 18rem;">
                        <img style="height:100px; width:200px" class="card-img-top mx-auto display-block" src="{{asset('img/autos').'/'.$auto->foto}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$auto->modelo}}</h5>
                            <p class="card-text">{{$auto->marca}}</p>
                            <a href="/autos/{{$auto->slug}}" class="btn btn-primary">Ver más</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endsection