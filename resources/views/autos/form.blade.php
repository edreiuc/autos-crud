<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('modelo','Modelo') !!}
            {!! Form::text('modelo',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('marca','Marca') !!}
            {!! Form::text('marca',null,['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('anio','Año') !!}
            {!! Form::text('anio',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('color','Color') !!}
            {!! Form::text('color',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('matricula','Matricula') !!}
            {!! Form::text('matricula',null,['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    {!! Form::label('foto','Foto') !!}
    {!! Form::file('foto') !!}
</div>