@extends('layouts.principal')
    @section('titulo','Nuevo auto')
    @section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['route'=>'autos.store', 'method'=>'POST','files'=>true]) !!}
        @include('autos.form')
        <div class="row">
            <div class="col-md-1">
               {!! Form::submit('Guardar', ['class'=>'btn btn-success']) !!}
            </div>
        </div>
    {!! Form::close() !!}
    @endsection