<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <title>Automovil - @yield('titulo')</title>
</head>
<body>
    <nav class="navbar navbar-dark bg-primary">
        <a href="/autos" class="navbar-brand">AUTOS</a>
    </nav>
    <br>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>